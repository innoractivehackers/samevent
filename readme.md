# Sam Event Checkin System

Keep track of event attendance using QR code.

SamEvent keep tracks of events
and attendees of each event. Each attendee is assigned an checkin URL, which
can be encoded as an QR code. Upon loading the URL in a browser, attendance will
be marked with a timestamp.

## Deployment

[Composer](https://getcomposer.org/) is required to install the necessary PHP library dependencies.

````bash
composer install
````

Copy *.env.example* to *.env*, edit to suit your environment. Then setup database using the following command

````bash
php artisan migrate
````

Note: You still need to manually create the first user account before you can login.
