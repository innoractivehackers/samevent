<?php

namespace App\Models;

class Checkin extends Model
{

	protected $fillable = ['name', 'checkin_time', 'company'];

	public function event()
	{
		return $this->belongsTo('App\Models\Event');
	}

	public static function rules()
	{
		return [
			'name' => 'required|max:255',
			'checkin_time' => 'date_format:U',
			'company' => 'max:255',
		];
	}

}
