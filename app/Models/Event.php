<?php

namespace App\Models;

use Illuminate\Contracts\Validation\Validator;

class Event extends Model
{

	protected $fillable = ['name', 'description'];

	public function checkins()
	{
		return $this->hasMany('App\Models\Checkin');
	}

	/**
	 * Validation rules.
	 * @return array
	 */
	public static function rules()
	{
		return [
			'name' => 'required|min:3|max:255',
		];
	}

}
