<?php

namespace App\Models;

abstract class Model extends \Illuminate\Database\Eloquent\Model {

	/** @override Unix timestamp format by default. */
	protected $dateFormat = 'U';

}
