<?php

namespace App\Http\Controllers;

use App\User;
use Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{

	public function edit(Request $request)
	{
		return view('user.edit', ['user' => $request->user()]);
	}

	public function update(Request $request)
	{
		$user = $request->user();
		
		$rules = User::rules();
		$rules['email'] = 'required|email|max:255|unique:users,email,'.$user->id;
		$this->validate($request, $rules);

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();

        return $this->flashSuccess('Profile update successfully', route('user.edit'));
	}

	public function password(Request $request)
	{
		return view('user.password');
	}

	public function updatePassword(Request $request)
	{
		$this->validate($request, [
			'password' => 'required|min:6|confirmed',
		]);

		$user = $request->user();
		$user->password = Hash::make($request->input('password'));
		$user->save();
		return $this->flashSuccess('Password updated successfully', route('user.password'));
	}

}
