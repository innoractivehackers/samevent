<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

	/**
	 * Flash success message in view.
	 * @param string $message
	 * @param string $uri Destination redirect to.
	 */
	protected function flashSuccess($message, $uri) {
		return redirect($uri)->with('flashSuccess', $message);
	}

	/**
	 * Flash error message in view.
	 * @param string $message
	 * @param string $uri Destination redirect to.
	 */
	protected function flashError($message, $uri) {
		return redirect($uri)->with('flashError', $message);
	}
}
