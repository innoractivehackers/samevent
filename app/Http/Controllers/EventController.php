<?php

namespace App\Http\Controllers;

use App\Models\Checkin;
use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * List events.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$events = Event::orderBy('name')->paginate(20);

        return view('event.index', compact('events'));
    }

    public function create()
    {
        return view('event.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, Event::rules());

        $event = Event::create($request->all());
        $event->save();

        return $this->flashSuccess('Event created successfully', route('event.show', $event));
    }

    public function edit(Event $event)
    {
        return view('event.edit', compact('event'));
    }

    public function update(Event $event, Request $request)
    {
        $this->validate($request, Event::rules());

        $event->name = $request->input('name');
        $event->description = $request->input('description');
        $event->save();

        return $this->flashSuccess('Event update successfully', route('event.show', $event));
    }

    public function show(Event $event)
    {
        $attendees = $event->checkins()->orderBy('name')->get();
        return view('event.show', compact('event', 'attendees'));
    }

    public function destroy(Event $event)
    {
        $event->delete();
        return $this->flashSuccess('Event deleted.', route('event.index'));
    }

    /**
     * Mass addition of checkins.
     */
    public function addAttendee(Event $event)
    {
        return view('event.addAttendee', compact('event'));
    }

    /**
     * Mass addition of checkins.
     */
    public function storeAttendee(Event $event, Request $request)
    {
        $attendees = explode("\n", $request->input('attendee'));
        $attendees = array_reduce($attendees, function($carry, $name) {
            $name = trim($name);
            if ($name != '')
            {
                $carry[] = new Checkin(['name' => $name]);
            }

            return $carry;
        }, []);

        if (!empty($attendees)) {
            $event->checkins()->saveMany($attendees);
            return $this->flashSuccess('Attendee(s) added.', route('event.show', $event));
        }

        return $this->flashError('No attendee added.', route('event.show', $event));
    }

    /**
     * AJAX call
     * Mass deletion of checkins.
     */
    public function deleteAttendees(Request $request)
    {
        Checkin::destroy($request->input('attendeeIds', []));
    }
}
