<?php

namespace App\Http\Controllers;

use App\Models\Checkin;
use App\Models\Event;
use Illuminate\Http\Request;

class QrController extends Controller
{
	/**
	 * Display all QR codes for an event, for printing purpose.
	 */
	public function printing(Event $event)
	{
		return view('qr.printing', ['attendees' => $event->checkins]);
	}

}
