<?php

namespace App\Http\Controllers;

use App\Models\Checkin;
use App\Models\Event;
use Illuminate\Http\Request;

class CheckinController extends Controller
{
	public function edit(Checkin $checkin)
	{
		return view('checkin.edit', compact('checkin'));
	}

	public function update(Checkin $checkin, Request $request)
	{
		$this->validate($request, Checkin::rules());
		$checkin->update($request->all());

        return $this->flashSuccess('Attendee updated successfully', route('event.show', $checkin->event_id));
	}

	/**
	 * Update checkin time
	 */
	public function checkin(Checkin $checkin)
	{
		$checkin->checkin_time = time();
		$checkin->save();

		return view('checkin.checkin', compact('checkin'));
	}

	/**
	 * Live display greeting for latest checkin
	 */
	public function watch(Event $event)
	{
		return view('checkin.watch', compact('event'));
	}

	/**
	 * AJAX get lastest checkin attendee info
	 */
	public function latestCheckin(Event $event)
	{
		$checkin = $event->checkins()
			->where('checkin_time', '>', 0)
			->orderBy('checkin_time', 'DESC')
			->first();

		return response()->json(is_null($checkin) ? null: $checkin->toArray());
	}

}
