<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group(['middleware' => 'auth'], function () {
	Route::get('/', 'EventController@index');

	/* Attendee */
	Route::get('event/{event}/add-attendee', ['as' => 'event.addAttendee', 'uses' => 'EventController@addAttendee']);
	Route::post('event/{event}/store-attendee', ['as' => 'event.storeAttendee', 'uses' => 'EventController@storeAttendee']);
	// AJAX call
	Route::post('event/delete-attendees', ['as' => 'event.deleteAttendees', 'uses' => 'EventController@deleteAttendees' ]);
	Route::resource('event', 'EventController');

	/* Checkin */
	Route::resource('checkin', 'CheckinController');
	// NOTE This route *MUST* go after `Route::resource('checkin', 'CheckinController')`
	Route::get('checkin/{checkin}', ['as' => 'checkin', 'uses' => 'CheckinController@checkin']);

	/* User */
	Route::get('user/edit', ['as' => 'user.edit', 'uses' => 'UserController@edit']);
	Route::put('user/update', ['as' => 'user.update', 'uses' => 'UserController@update']);
	Route::get('user/password', ['as' => 'user.password', 'uses' => 'UserController@password']);
	Route::post('user/update-password', ['as' => 'user.updatePassword', 'uses' => 'UserController@updatePassword']);

	/* QR */
	Route::get('qr/printing/{event}', ['as' => 'qr.printing', 'uses' => 'QrController@printing']);
});

Route::group(['prefix' => 'checkin'], function() {
	// Live display greeting for latest checkin
	Route::get('watch/{event}', ['as' => 'checkin.watch', 'uses' => 'CheckinController@watch']);
	// AJAX get lastest checkin attendee info
	Route::any('latest/{event}', ['as' => 'checkin.latest', 'uses' => 'CheckinController@latestCheckin']);
});
