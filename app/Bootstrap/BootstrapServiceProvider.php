<?php

namespace App\Bootstrap;

use Collective\Html\FormFacade as Form;
use Illuminate\Support\ServiceProvider;

class BootstrapServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Form::component('bsUrlButton', 'bootstrap.urlButton', ['url', 'name', 'type' => 'primary', 'attributes' => []]);
        Form::component('bsText', 'bootstrap.form.text', ['name', 'value', 'attributes' => []]);
        Form::component('bsEmail', 'bootstrap.form.email', ['name' => 'email', 'value', 'attributes' => []]);
        Form::component('bsPassword', 'bootstrap.form.password', ['name' => 'password', 'attributes' => []]);
        Form::component('bsTextarea', 'bootstrap.form.textarea', ['name', 'value', 'attributes' => []]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
