<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
	{{ Form::label($name, (isset($attributes['label']) ? $attributes['label'] : null), ['class' => 'control-label']) }}
	{{ Form::password($name, array_merge(['class' => 'form-control'], $attributes)) }}
	@if ($errors->has($name))
		<span class="help-block">{{ $errors->first($name) }}</span>
	@endif
</div>
