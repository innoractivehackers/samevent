@extends('layouts.master')

@section('title', 'Attendee Checkin')

@section('content')
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="jumbotron text-center">
				<h1 class="text-primary">{{ $checkin->name }}</h1>

				@if ($checkin->company)
					<h2 class="text-info">{{ $checkin->company }}</h2>
				@endif

				<p>Successfully checkin to {{ link_to_route('event.show', $checkin->event->name, [$checkin->event]) }}!</p>
			</div>
		</div>
	</div>
@endsection
