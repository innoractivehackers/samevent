@extends('layouts.master')

@section('title', 'Edit '.$checkin->name)

@section('content')
	<h1>Edit <em>{{ $checkin->name }}</em></h1>

	<div class="row">
		<div class="col-xs-6">
			{{ Form::model($checkin, ['route' => ['checkin.update', $checkin], 'method' => 'PUT']) }}

				{{ Form::bsText('name', null, ['label' => 'Name*', 'autofocus']) }}
				{{ Form::bsText('company', null) }}

				{{ Form::submit('Save', ['class' => 'btn btn-primary']) }}

			{{ Form::close() }}
		<div>
	<div>
@endsection
