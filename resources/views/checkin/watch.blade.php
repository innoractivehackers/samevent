<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{ $event->name }} | SamEvent</title>

    <!-- Bootstrap -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<meta name="csrf-token" content="{{ csrf_token() }}">
  </head>
  <body>

	<div id="watch-attendee" class="container text-center text-primary">
		<div class="large-display">Welcome</div>
		<div class="large-display text-danger" id="attendee-name"></div>
		<div class="large-display text-danger" id="attendee-company"></div>
		<div class="large-display">to</div>
		<div class="large-display text-warning" id="event-name">{{ $event->name }}</div>

		<p class="large section-margin">{{ $event->description }}</p>
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script>
	$(document).ready(function() {
		var watchAttendee = function() {
			$.getJSON("{{ route('checkin.latest', [$event]) }}")
				.done(function(data) {
                    if (data.checkin_time < (Date.now() / 1000) - 300) {
                        $('#attendee-name').text('');
                        $('#attendee-company').text('');
                    }
                    else {
                        $('#attendee-name').text(data.name);
                        if (data.company) {
                            $('#attendee-company').text('['+data.company+']');
                        }
                        else {
                            $('#attendee-company').text('');
                        }
                    }
				})
				.always(function() {
					setTimeout(watchAttendee, 1000);
				});
		};

		// Start monitoring at 1s interval
		watchAttendee();
	});
	</script>
  </body>
</html>
