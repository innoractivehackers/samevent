@extends('layouts.master')

@section('title', $event->name)

@section('content')
	<h1>{{ $event->name }}</h1>

	<p class="text-info">{{ $event->description }}</p>

	<p>URL: <a href="{{ route('checkin.watch', [$event]) }}">{{ route('checkin.watch', [$event]) }}</a></p>

	<h2>Attendees</h2>

	<div class="row">
		<div class="col-md-12">
			{{ Form::bsUrlButton(route('event.addAttendee', [$event]), 'Add Attendee') }}
			<button class="btn btn-primary" id="select-all">Select All</button>
			<button class="btn btn-primary" id="unselect-all">Unselect All</button>
			<button class="btn btn-danger" id="delete-selected">Deleted Selected</button>
			{{ Form::bsUrlButton(route('qr.printing', [$event]), 'Print QR', 'primary', ['target' => '_blank']) }}
		</div>
	</div>

	<table class="table table-condensed table-hover table-bordered section-margin">
		<tr class="info">
			<th></th>
			<th>Name</th>
			<th>Company</th>
			<th>Arrival</th>
			<th>Action</th>
		</tr>

		@foreach ($attendees as $attendee)
			<tr>
				<td class="attendee-select"><input type="checkbox" class="attendee-id" value="{{ $attendee->id }}"></td>
				<td>{{ $attendee->name }}</td>
				<td>{{ $attendee->company }}</td>
				<td>
					@if ($attendee->checkin_time)
						{{ date('Y-m-d H:i:s', $attendee->checkin_time) }}
					@else
						<span class="glyphicon glyphicon-remove text-danger"></span>
					@endif
				</td>
				<td>
					{{ link_to_route('checkin.edit', 'Edit', [$attendee]) }}
					| <a href="https://chart.googleapis.com/chart?cht=qr&chs=500x500&chld=L|1&chl={{ route('checkin', [$attendee]) }}" target="_blank">QR Code</a>
				</td>
			</tr>
		@endforeach
	</table>

	<div class="row section-margin">
		<div class="col-xs-12 text-right">
			{{ link_to('#', 'Delete Event', ['class' => 'text-muted', 'data-toggle' => 'modal', 'data-target' => '#delete-event']) }}
		</div>
	</div>

	<div class="modal fade" id="delete-event" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Delete Event</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete this event?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="confirm-delete-event">Yes</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
			</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div class="modal fade" id="delete-dialog" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Delete Attendee(s)</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete the selected attendee(s)?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" id="confirm-delete">Yes</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
			</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
@endsection

@section('footerScript')
	<script>
	$(document).ready(function() {
		var numAttendees = $('.attendee-id').length;

		// https://laravel.com/docs/5.2/routing#csrf-x-csrf-token
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		// Toggle the (un)select-all buttons
		function checkboxChanged() {
			if ($('.attendee-id:checked').length == 0) {
				$('#unselect-all').prop('disabled', true);
				$('#delete-selected').prop('disabled', true);
			}
			else {
				$('#unselect-all').prop('disabled', false);
				$('#delete-selected').prop('disabled', false);
			}

			if ($('.attendee-id:checked').length == numAttendees) {
				$('#select-all').prop('disabled', true);
			}
			else {
				$('#select-all').prop('disabled', false);
			}
		}

		$('#unselect-all')
			.prop('disabled', true)
			.click(function(event) {
				$('.attendee-id').prop('checked', false);
				checkboxChanged();
			});

		$('#select-all')
			.click(function(event) {
				$('.attendee-id').prop('checked', true);
				checkboxChanged();
			});

		$('#delete-selected').click(function(event) {
			$('#delete-dialog').modal('show');
		});

		$('#confirm-delete').click(function(event) {
			var attendeeIds =  $.map($('.attendee-id:checked'), function(ele) {
				return $(ele).val();
			});

			$.post(
				"{{ route('event.deleteAttendees') }}",
				{'attendeeIds': attendeeIds},
				function(data, textStatus, jqXHR) {
					location.href = "{{ route('event.show', $event) }}";
				}
			);
		});

		$('#confirm-delete-event').click(function() {
			$.ajax({
				method: 'DELETE',
				url: "{{ route('event.destroy', [$event]) }}",
				complete: function(jqXHR, textStatus) {
					location.href = "{{ route('event.index') }}";
				}
			});
		});

		$(".attendee-id").change(function(event) {
			checkboxChanged();
		});

		// Init
		checkboxChanged();
	});
	</script>
@endsection
