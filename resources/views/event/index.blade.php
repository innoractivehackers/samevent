@extends('layouts.master')

@section('title', 'Events')

@section('content')
	<h1>Events</h1>

	<div class="row">
		<div class="col-xs-12">
			{{ Form::bsUrlButton(route('event.create'), 'Create Event') }}
		<div>
	<div>

	@if (count($events) == 0)
		<p class="text-info">No event created yet. {{ link_to_route('event.create', 'Create') }} one now.</p>
	@else
		<table class="table table-hover table-bordered section-margin">
			<tr class="info">
				<th>Name</th>
				<th>Description</th>
				<th></th>
			</tr>

			@foreach ($events as $event)
				<tr>
					<td>{{ link_to_route('event.show', $event->name, [$event]) }}</td>
					<td>{{ $event->description }}</td>
					<td>{{ link_to_route('event.edit', 'Edit', [$event]) }}</td>
				</tr>
			@endforeach
		</table>
	@endif

	{{ $events->links() }}
@endsection
