@extends('layouts.master')

@section('title', 'Edit Event')

@section('content')
	<h1>Edit Event</h1>

	<div class="row">
		<div class="col-md-6">
			{{ Form::model($event, ['route' => ['event.update', $event], 'method' => 'PUT']) }}

			@include('event._event_form')

			{{ Form::close() }}
		<div>
	<div>
@endsection
