@extends('layouts.master')

@section('title', 'Add Attendee')

@section('content')
	<h1>Add Attendee for <em>{{ $event->name }}</em></h1>

	<div class="row">
		<div class="col-xs-6">
			{{ Form::open(['route' => ['event.storeAttendee', 'event' => $event]]) }}

				<div class="form-group">
					<label for="attendee">Attendee</label>
					<textarea id="attendee" name="attendee" class="form-control" rows="10" required autofocus>{{ old('attendee') }}</textarea>
					<p class="help-block">Specify attendee names, 1 name per line. Use 'SHIFT + ENTER' for additional line.</p>
				</div>

				{{ Form::submit('Save', ['class' => 'btn btn-primary']) }}

			{{ Form::close() }}
		<div>
	<div>
@endsection
