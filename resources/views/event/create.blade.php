@extends('layouts.master')

@section('title', 'Create Event')

@section('content')
	<h1>Create Event</h1>

	<div class="row">
		<div class="col-md-6">
			{{ Form::open(['route' => 'event.store']) }}

			@include('event._event_form')

			{{ Form::close() }}
		<div>
	<div>
@endsection
