<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Print QR Codes | SamEvent</title>

    <!-- Bootstrap -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
        <div class="qr-container clearfix">
            @foreach($attendees as $attendee)
                <div class="qr-card">
                    <img src="https://chart.googleapis.com/chart?cht=qr&chs=500x500&chld=L|1&chl={{ urlencode(route('checkin', [$attendee])) }}" alt="{{ $attendee->name }}">
                    <div class="caption text-center">{{ $attendee->name }}</div>
                </div>
            @endforeach
        </div>
  </body>
</html>
