@extends('layouts.master')

@section('title', 'Update Profile')

@section('content')
	<h1>Update Profile</h1>

	<div class="row">
		<div class="col-md-6">
			{{ Form::model($user, ['route' => ['user.update', $user], 'method' => 'PUT']) }}

			{{ Form::bsText('name', null, ['label' => 'Name*', 'autofocus']) }}
			{{ Form::bsEmail('email', null, ['label' => 'Email*',]) }}
			{{ Form::submit('Save', ['class' => 'btn btn-primary']) }}

			{{ Form::close() }}
		<div>
	<div>
@endsection
