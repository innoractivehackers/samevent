@extends('layouts.master')

@section('title', 'Change Password')

@section('content')
	<h1>Change Password</h1>

	<div class="row">
		<div class="col-md-6">
			{{ Form::open(['route' => ['user.updatePassword']]) }}

			{{ Form::bsPassword('password', ['label' => 'Password*', 'autofocus']) }}
			{{ Form::bsPassword('password_confirmation', ['label' => 'Password Confirmation*']) }}
			{{ Form::submit('Save', ['class' => 'btn btn-primary']) }}

			{{ Form::close() }}
		<div>
	<div>
@endsection
