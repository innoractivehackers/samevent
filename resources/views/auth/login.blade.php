@extends('layouts.master')

@section('title', 'Login')

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center">
            <h1>Login</h1>

            <p class="text-info">Please login to continue.</p>

            {{ Form::open(['url' => url('login')]) }}
                <input type="hidden" name="remember" value="on">

                {{ Form::bsEmail('email', null, ['autofocus']) }}
                {{ Form::bsPassword() }}
                {{ Form::submit('Login', ['class' => 'btn btn-primary']) }}

                <div><a href="{{ url('/password/reset') }}">Forgot Your Password?</a></div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
